# PDF Latex Lambda 

This is a Lambda runtime of the [Docker TexLive Full image](https://github.com/thomasWeise/docker-texlive-full) by Thomas Weise.  It is tagged accordingly to match versioning of its base image.